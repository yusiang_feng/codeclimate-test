import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd


sns.set_theme(style="whitegrid")

data = {
    "Test Data":  ['#1', '#2', '#3', '#4', '#5', '#6', '#7',
                   '#1', '#2', '#3', '#4', '#5', '#6', '#7',
                   '#1', '#2', '#3', '#4', '#5', '#6', '#7'
                   ],
    "Time (sec)": [0.2977, 0.5632, 0.2645, 0.8241, 0.2078, 0.4423, 15.6025,
                   3.7035, 7.2113, 3.3311, 10.343, 2.6078, 5.6588, 199.4321,
                   0.0431, 0.081, 0.0384, 0.1214, 0.0301, 0.0634, 2.254
                   ],

    "Methods": ['Learning GBP', 'Learning GBP', 'Learning GBP', 'Learning GBP', 'Learning GBP', 'Learning GBP', 'Learning GBP',
                'GBP-200', 'GBP-200', 'GBP-200', 'GBP-200', 'GBP-200', 'GBP-200', 'GBP-200',
                'GBP-2', 'GBP-2', 'GBP-2', 'GBP-2', 'GBP-2', 'GBP-2', 'GBP-2'
               ]
}

data = pd.DataFrame(data)
ax = sns.barplot( x="Test Data", y="Time (sec)", hue='Methods', data=data)
ax.lines[0].set_linestyle("--")
ax.lines[1].set_linestyle("--")

ax.xaxis.grid(False)
# ax.yaxis.grid(True, which="major", linestyle="-")
# ax.yaxis.grid(True, which="minor", linestyle="-")

plt.legend(loc='upper left')
plt.yscale('log')
plt.savefig('compare_GBP_time.pdf')
plt.show()
